import {display} from './reducers/display'

import {combineReducers} from 'redux'

const appStore = combineReducers({
    "display": display
});

export default appStore;
