'use strict'

import React, { Component } from 'react'
import { render } from 'react-dom'
import { connect } from 'react-redux'

export default class Welcome extends Component {
  constructor(props) {
    console.log('App.props = ', props)
    super(props);
  }
  render() {
    return (
      <div>
        <p>Welcome to React Js and Redux</p>
      </div>
    )
  }
}