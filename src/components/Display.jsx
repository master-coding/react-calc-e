'use strict'

import React, { Component } from 'react'
import { render } from 'react-dom'
import { connect } from 'react-redux'

export default class Display extends Component {
  constructor(props) {
    console.log(props);
    super(props);
  }
  componentWillMount(){
    console.log("Dispaly : componentWillMount")
  }
  componentDidMount(){
    console.log("Dispaly : componentDidMount")
  }

  componentWillReceiveProps() {
    console.log("Display: componentWillReceiveProps");
  }
  shouldComponentUpdate() {
    console.log("Display: shouldComponentUpdate");
    return true;
  }
  componentWillUpdate() {
    console.log("Display: componentWillUpdate");
  }
  componentDidUpdate() {
    console.log("Display: componentDidUpdate");
  }
  componentWillUnmount(){
    console.log("Display: componentWillUnmount");
  }
  render() {
    console.log("Display : render");
    return (
      <div id="display">
        <input id="display" type="text" value={this.props.value.result !== 0 ? this.props.value.result : this.props.value.expression} readOnly/>
      </div>
    )
  }
}