"use strict";

import React, { Component } from "react";
import { render } from "react-dom";
import { connect } from "react-redux";
import Key from "./Key.jsx";

export default class KeyPad extends Component {
  constructor(props) {
    console.log("Key Pad props = ", props);
    super(props);
  }

  keyPressedCallBackFunction(key){
    console.log("The key received in Keypad from Key is ", key);
  }

  render() {
    return (
      <div>
        {
          this.props.keys.map((value) => {
            return <Key key={`k${value}`} keyText={value} dispatch={this.props.dispatch} callBack={this.keyPressedCallBackFunction}/>
          })
        }
      </div>
    );
  }
}