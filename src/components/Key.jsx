'use strict'

import React, { Component } from 'react'
import { render } from 'react-dom'
import { connect } from 'react-redux'

export default class Key extends Component {
  constructor(props) {
    console.log(props);
    super(props);
    this.keyClick = this.keyClick.bind(this);
  }
  keyClick(event){
    console.log("Key is pressed", event.target.innerText);
    console.log(this.props);
    this.props.callBack(event.target.innerText);
    this.props.dispatch({
      "type": "KEY_PRESSED",
      "myKey" : event.target.innerText
    });

    let className = this.refs.key.getAttribute("class");
    this.refs.key.setAttribute("class", className + " highlight");
  }
  render() {
    return (
      <div>
        <button ref={"key"} onClick={this.keyClick} className={"key"}>{ this.props.keyText}</button>
      </div>
    )
  }
}