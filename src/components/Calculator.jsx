'use strict'

import React, { Component } from 'react'
import { render } from 'react-dom'
import { connect } from 'react-redux'
import Display from "./Display.jsx"
import KeyPad from "./KeyPad.jsx"

export class Calculator extends Component {
  constructor(props) {
    console.log("Calculator props ", props);
    super(props);
  }
  render() {
    return (
      <div id="calculation-division">
        <Display value={this.props.display}/>
        <KeyPad keys={["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "+", "-", "*", "/", "=", "C"]} dispatch={this.props.dispatch}/>
      </div>
    )
  }
}

function select(store) {
  console.log("connecting with Calculator component", store);
  return store;
}

export default connect(select)(Calculator);