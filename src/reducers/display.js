import * as math from 'mathjs';

export function display(state={result: 0, expression: ""}, action = {}) {
  console.log("display reducer", state);
  switch (action.type) {
    case "KEY_PRESSED": {
      console.log("inside key pressed", state);
      let result = 0;
      let expression = state.expression;
      if(action.myKey === "="){
        console.log("inside equal");
        result = math.eval(state.expression);
      }else if(action.myKey === "C"){
        return {result: 0, expression: ""};
      }else{
        expression = expression + action.myKey;
      }
      return { result: result, expression: expression }
    }
  }
  return state;
}